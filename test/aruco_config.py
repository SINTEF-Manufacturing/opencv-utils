# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import math3d as m3d
import cv2

import opencv_utils as ou
from opencv_utils.cam_spec import CSSerial
from opencv_utils.parameters import (CalibrationParameters,
                                     CaptureParameters)

from opencv_utils.aruco import ArUcoLayout, ArUcoPose, ArUcoParameters

# Logitech 9000 Pro
#cam_spec = CSSerial('446B4B9D')

# Logitech C930e
cam_spec = CSSerial('15DEA67E')

# Integrated web-cam
# cam_id = 'usb-Sunplus_IT_Co_Lenovo_EasyCamera-video-index0'

cap_pars = CaptureParameters(
    shape=(800,1200),
    # shape=(600, 800),
    fps=10,
    mono=True,
    saturation=0,
    focus=10,
    zoom=0,
    autofocus=0.0,
    auto_wb=0,
    wb_temperature=5000,
    temperature=5000,
    hue=-1,
    sharpness=0,
    brightness=100,
    contrast=150,
    exposure=100,
    gain=10,
    gamma=-1,
    aperture=-1,
    auto_exposure=0.0,
)


cal_pars = CalibrationParameters((6, 8), 11.5)

tag_size = 28.5  # mm

aruco_layout = ArUcoLayout()
aruco_layout += ArUcoPose(id=0,
                          AAi=m3d.Transform(m3d.Orientation(),
                                            m3d.Vector(0, 0, 0)),
                          tag_size=tag_size)
aruco_layout += ArUcoPose(id=2,
                          AAi=m3d.Transform(m3d.Orientation(),
                                            m3d.Vector(256, 0, 0)),
                          tag_size=tag_size)
aruco_layout += ArUcoPose(id=4,
                          AAi=m3d.Transform(m3d.Orientation(),
                                            m3d.Vector(128, 0, 0)),
                          tag_size=tag_size)
aruco_layout += ArUcoPose(id=1,
                          AAi=m3d.Transform(m3d.Orientation(),
                                            m3d.Vector(0, 172, 0)),
                          tag_size=tag_size)
aruco_layout += ArUcoPose(id=3,
                          AAi=m3d.Transform(m3d.Orientation(),
                                            m3d.Vector(256, 172, 0)),
                          tag_size=tag_size)
aruco_layout += ArUcoPose(id=5,
                          AAi=m3d.Transform(m3d.Orientation(),
                                            m3d.Vector(128, 172, 0)),
                          tag_size=tag_size)

aruco_pars = ArUcoParameters(aruco_dict_code=cv2.aruco.DICT_6X6_250,
                             aruco_layout=aruco_layout)
