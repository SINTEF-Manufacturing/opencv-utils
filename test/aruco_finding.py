# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import argparse

import numpy as np
import cv2
import math3d as m3d

from opencv_utils import Capture  # , CaptureViewer
from opencv_utils.aruco import ArUcoFinder
from opencv_utils.calibration import Calibration

from opencv_utils.config_loader import load_config


ap = argparse.ArgumentParser()
ap.add_argument('--cap', action='store_true')
args = ap.parse_args()

cfg = load_config('aruco_config.py')

cal = Calibration('aruco_cal')

if args.cap:
    c = Capture(cfg.cam_id, cfg.cap_pars)
    c.start()
    img = c.capture()
else:
    img = cv2.imread('aruco.bmp')

af = ArUcoFinder(cfg.aruco_pars.aruco_dict, cal)

marked_img, CA, aruco_locs = af(img, poses_3d=True,
                                aruco_layout=cfg.aruco_layout)

cv2.imshow('ArUco', marked_img)

cv2.waitKey(-1)

for a in aruco_locs:
    ACCAipos = CA.inverse*a.CAi.pos
    AAipos = cfg.aruco_layout[a.id].AAi.pos
    print(a.id, AAipos, ACCAipos, AAipos.dist(ACCAipos))
