# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2024"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import cv2 as cv

from opencv_utils import CVImageViewer

i = cv.imread('aruco.bmp')

iv1 = CVImageViewer()
iv2 = CVImageViewer()

iv1.image = i
iv2.image = i
