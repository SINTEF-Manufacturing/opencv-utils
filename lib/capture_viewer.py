# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2024"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import logging

from . import Capture

class CaptureViewer(threading.Thread):

    def __init__(self,
                 capture, image_viewer,
                 window_name=None,
                 mouse_events=True):
        if window_name is None:
            window_name = id(self)
        threading.Thread.__init__(self,
                                  name=f'CV<{window_name}>',
                                  daemon=True)
        self._log = logging.getLogger(self.name)
        self._cap = capture
        self._cap_lock = threading.Lock()
        self._post_procs = []
        self._running = threading.Event()
        self._running.set()
        self._stop_flag = False
        self._iv = image_viewer

    def __del__(self):
        self._log.info('Closing capture viewer')
        self.stop()

    def set_cap(self, cap: Capture):
        with self._cap_lock:
            self._cap.stop()
            self._cap = cap
            self.resume()
        
    def wait_for_mouse(self):
        self._iv.wait_for_mouse()

    @property
    def mouse_click_pos(self):
        return self._iv._mouse_click_pos

    @property
    def image(self):
        return self._iv._image

    @property
    def paused(self):
        return not self._running.is_set()

    @property
    def running(self):
        return self._running.is_set()

    def pause(self):
        self._running.clear()

    def resume(self):
        self._running.set()

    def stop(self, join=True):
        self._iv.stop()
        self._stop_flag = True
        if join:
            self.join()

    def run(self):
        while not self._stop_flag:
            self._running.wait()
            if self._cap is None:
                self.pause()
                continue
            with self._cap_lock:
                f = self._cap.capture()
            if f is None:
                continue
            frames = [f]
            for pp in self._post_procs:
                frames.append(pp(frames[-1])[-1])
            self._iv.set_image(frames[-1])
