# coding=utf-8

"""
From https://www.authentise.com/post/detecting-circular-shapes-using-contours
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import sys
import dataclasses
import typing
import logging

import cv2
import skimage.measure
import numpy as np

from .canny_edge_detector import CannyEdgeDetector

# rawImage = cv2.imread('circles2.jpg')
# cv2.imshow('Original Image', rawImage)
# cv2.waitKey(0)


@dataclasses.dataclass
class EllipsesFinder:
    thr1: int = 50
    thr2: int = 100
    sigma: float = 0.5
    min_contour_cardinality: int = 50
    min_contour_size: int = 20  # Lower characteristic size in pixels
    max_contour_size: int = 50  # Upper bound on characteristic size in pixels
    res_max: float = 10.0
    smooth: int = 5
    dilate: int = 0
    log_level: int = logging.WARN
    logger_name: str = 'EllipsesFinder'

    def __post_init__(self):
        self.log = logging.getLogger(self.logger_name)
        self.log.setLevel(self.log_level)
        self.canny_filter = CannyEdgeDetector(self.thr1, self.thr2, self.sigma)

    class EllipsesResult(typing.NamedTuple):
        ellipses_img: np.ndarray
        contours_img: np.ndarray
        canny_img: np.ndarray
        contours: typing.List[np.ndarray]  # Effective shape (n,m_i,2)
        ellipses_contours: typing.List[np.ndarray]  # Effective shape (n,m_i,2)
        ellipses: typing.List[skimage.measure.EllipseModel]

    def find_ellipses_in_contours(self, contours):
        contour_list = []
        econtour_list = []
        unecont_list = []
        ellipses = []
        for contour in contours:
            # cont = contour.reshape((-1,2)).astype(np.float)
            # approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
            approx = cv2.approxPolyDP(contour, 2, False)# .reshape(-1,2).astype(np.float32)
            #area = cv2.contourArea(contour)
            #if ((len(approx) > 8) & (area > 30) ):
            #if len(approx) / len(contour) > 0.1 and len(approx) > 10:
            c_card = len(contour)
            c_rect = cv2.boundingRect(contour)
            c_size = max(c_rect[2:])
            if (c_card > self.min_contour_cardinality and
                c_size > self.min_contour_size and
                c_size < self.max_contour_size):
                # e = skimage.measure.EllipseModel()
                # if e.estimate(contour):
                rres = skimage.measure.ransac(
                    contour[::3], skimage.measure.EllipseModel,
                    min_samples=5, # int(0.25 * contour.shape[0]),
                    # max_trials=20,
                    residual_threshold=2,
                    # stop_sample_num=int(0.25 * contour.shape[0])
                )
                if rres is not None:
                    e, inliers = rres
                    if e is None:
                        self.log.info(
                            f'EllipsesFinder.find_ellipses_in_contours: ' +
                            f'Encountered a "None" model.')
                        continue
                    e.xc, e.yc, e.b, e.a, theta = e.params
                    e.centre = np.array([e.xc, e.yc])
                    if e.b > e.a:
                        self.log.warn(f'EllipsesFinder.find_ellipses_in_contours: WRONG a ({e.a}) < b ({e.b})')
                        tmp = e.b
                        e.b = e.a
                        e.a = tmp
                    else:
                        self.log.debug(
                            f'EllipsesFinder.find_ellipses_in_contours: '
                            f'CORRECT a ({e.a}) >= b ({e.b})')
                    e.ecc = np.sqrt(1-(e.b/e.a)**2) ** 2
                    ress = e.residuals(contour)
                    res_avg = np.average(ress)
                    res_max = np.max(ress)
                    if res_max < self.res_max: # and e.ecc < 0.9: # and e.b > 20 and e.a < 1000:
                        #print(e.params)
                        #print(e.ecc)
                        #print(res_avg, res_max)
                        contour_list.append(contour)
                        ellipses.append(e)
                        econtour_list.append(e.predict_xy(
                            np.linspace(0, 2*np.pi, 100)).reshape(-1, 1, 2)
                                             .astype(np.int32).reshape((-1, 2)))
                else:
                    unecont_list.append(contour)

        return ellipses, econtour_list

    def find_contours(self, canny_image: np.ndarray):
        contours, _ = cv2.findContours(canny_image,
                                       cv2.RETR_LIST,  # cv2.RETR_TREE,
                                       cv2.CHAIN_APPROX_SIMPLE)
        return [c.reshape((-1, 2)).astype(np.float32) for c in contours]

    def draw_contours(self, img, contours):
        if len(img.shape) != 3:
            contours_img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        else:
            contours_img = img.copy()
        for c in contours:
            self.log.debug(f'draw_contour: shape {c.shape}')
            cv2.drawContours(contours_img, [c.astype(np.int64)],
                             -1, (0, 255, 0), 1)
        return contours_img

    def find_ellipses_in_image(self, img,
                               draw_contours=True,
                               draw_ellipses=True):
        img = img.copy()
        if self.smooth > 0:
            img = cv2.bilateralFilter(img, self.smooth, 175, 175)
        canny_img = self.canny_filter(img)
        if self.dilate > 0:
            canny_img = cv2.dilate(
                canny_img, cv2.getStructuringElement(
                    cv2.MORPH_RECT,
                    (self.dilate, self.dilate)))
        contours = self.find_contours(canny_img)
        ellipses, e_contours = self.find_ellipses_in_contours(contours)
        if draw_contours:
            contours_img = self.draw_contours(img, contours)
        else:
            contours_img = None
        if draw_ellipses:
            ellipses_img = self.draw_contours(img, e_contours)
        else:
            ellipses_img = None
        return self.EllipsesResult(
            contours,
            e_contours,
            ellipses,
            canny_img,
            contours_img,
            ellipses_img,
        )

    __call__ = find_ellipses_in_image
