# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math3d as m3d
import math3d.geometry as m3dg

from ..calibration import Calibration
from ..ray_getter import RayGetter


class PlaneProjector:
    def __init__(self,
                 cal: Calibration,
                 Cplane: m3dg.Plane,  # Plane in camera reference
                 PC: m3d.Transform  # Transform from camera to plane reference
                 ):
        self.rg = RayGetter(cal=cal)
        self.Cplane = Cplane
        self.PC = PC

    def __call__(self,
                 points: np.ndarray,  # Nx2 points in pixel coordinates
                 ref: str = 'camera'  # Reference for the returned points, 'plane' or 'camera'
                 ) -> np.ndarray:  # Nx3 points on the plane in plane or camera reference
        """Project the given pixel coordinate points to points on the
        plane. The points returned are in plane camera reference or
        plane reference based on the 'ref' option.
        """
        Cpoints = np.array([self.Cplane.intersection(ray).array
                            for ray in self.rg.get_ray_distorted(points)])
                           
        if ref == 'camera':
            return Cpoints
        elif ref == 'plane':
            Ppoints = (self.PC * Cpoints.T).T
            return Ppoints
        else:
            raise Exception(
                'PlaneProjector.project: ' +
                '"ref" argument must be "plane" or "camera"!')

