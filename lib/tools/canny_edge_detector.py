# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import dataclasses

import cv2
import numpy as np


@dataclasses.dataclass
class CannyEdgeDetector:
    thr1: int = 50
    thr2: int = 150
    sigma: float = 0.5

    def __call__(self, img):
        if self.sigma > 0.0:
            # Apply automatic Canny edge detection using the computed median
            median = np.median(img)
            self.thr1 = int(max(0, (1.0 - self.sigma) * median))
            self.thr2 = int(min(255, (1.0 + self.sigma) * median))
        # edged = cv2.Canny(img, lower, upper,L2gradient=True)
        return cv2.Canny(img, self.thr1, self.thr2, L2gradient=True)
