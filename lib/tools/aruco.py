# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import dataclasses
import typing

import numpy as np
import math3d as m3d
import cv2.aruco

from opencv_utils.calibration import Calibration


@dataclasses.dataclass
class ArUcoLoc:
    """Class for holding data of a camera-located ArUco tag."""
    id: int  # The ID of the ArUco tag
    Pcorners: np.ndarray  # List of four pixel coordinates for the corners
    CAi: m3d.Transform = None  # 3D pose of the ArUco tag in camera reference

    def __post_init__(self):
        # Correct for packing of corners in extra dimension
        if len(self.Pcorners.shape):
            if self.Pcorners.shape[0] == 1:
                self.Pcorners = self.Pcorners[0]
            else:
                raise Exception(
                    'ArUcoLoc: Unaccepted shape for Pcorners: ' +
                    f'{self.Pcorners.shape}')
        self.Pcentre = self.Pcorners.mean(axis=0)


@dataclasses.dataclass
class ArUcoPose:
    id: int
    # The pose of the tag (Ai) with reference to external (A)
    # coordinates. The origo of the tag is at corner of index 0. The
    # x-direction is towards the corner of index 3 and the y-direction
    # is towards the corner of index 1.
    AAi: m3d.Transform
    tag_size: float = None

    @property
    def corners(self) -> list[m3d.PositionVector]:
        # Return a list of corner position vectors in external
        # reference.
        return([m3d.PositionVector(c) for c in self.corners_array])

    @property
    def corners_array(self) -> np.ndarray:
        """Return the four corners traversed from origo corner and clockwise
        around. Corners are stored as row vectors, and an array of
        shape (4,3) is returned.
        """
        if self.tag_size is None:
            raise Exception('ArUcoPose: Can not calculate ' +
                            'corners_array without "tag_size"!')
        return np.array([self.AAi @ v for v in self.tag_size * np.array(
            [[0, 0, 0],
             [0, 1, 0],
             [1, 1, 0],
             [1, 0, 0]])])

    @property
    def centre(self) -> m3d.PositionVector:
        """Get the centre in A reference."""
        return m3d.PositionVector(self.centre_array)

    @property
    def centre_array(self) -> np.ndarray:
        """Get the centre in A reference."""
        return self.corners_array.mean(axis=0)


class ArUcoLayout(typing.Dict[int, ArUcoPose]):

    def add_pose(self, aruco_pose: ArUcoPose):
        self[aruco_pose.id] = aruco_pose

    def __iadd__(self, aruco_pose: ArUcoPose):
        self.add_pose(aruco_pose)
        return self


class ArUcoFinder:

    class ArUcoResult(typing.NamedTuple):
        aruco_img: np.ndarray
        CAi: m3d.Transform  # ArUco tag to camera transform
        aruco_locs: typing.Tuple[ArUcoLoc]

    def __init__(self, aruco_dict,
                 cal: Calibration = None):
        self._cal = cal
        self._ad = aruco_dict
        self._ap = cv2.aruco.DetectorParameters_create()
        self._ap.cornerRefinementMethod = cv2.aruco.CORNER_REFINE_SUBPIX

    def __call__(self, img,
                 poses_3d: bool = True,
                 aruco_layout: ArUcoLayout = None):
        corner_sets, ids, rejects = cv2.aruco.detectMarkers(img, self._ad,
                                                            parameters=self._ap)
        aimg = img.copy()
        if len(corner_sets) == 0:
            return self.ArUcoResult(aimg, None, [])
        ids.shape = -1
        cv2.aruco.drawDetectedMarkers(aimg, corner_sets, ids)
        # Draw markers on the first two corners of every tag.
        for corner_set in corner_sets:
            # Note: A corner set is doubly wrapped!
            corner_set = corner_set[0]
            # Get integer corners for drawing markers
            icorner_set = [tuple([int(c) for c in corners])
                           for corners in corner_set]
            # Measure ArUco diagonal for scaling markers
            diag = np.linalg.norm(corner_set[0] - corner_set[2])
            # Draw markers for 0'th and 1'st corners
            cv2.drawMarker(aimg, icorner_set[0], (255, 0, 0),
                           markerType=cv2.MARKER_CROSS, markerSize=int(diag/4))
            cv2.drawMarker(aimg, icorner_set[1], (0, 255, 0),
                           markerType=cv2.MARKER_DIAMOND, markerSize=int(diag/4))
        if poses_3d:
            # Identify the aruco reference A and individual aruco
            # references in camera reference C; CA and CAi
            # respectively.
            if self._cal is not None and self._cal.is_valid:
                # Identify individual markers in camera reference
                CAis = []
                for corner_set, id in zip(corner_sets, ids):
                    tag_size = aruco_layout[id].tag_size
                    rvecs, tvecs, obj_poss = cv2.aruco.estimatePoseSingleMarkers(
                        [corner_set], tag_size,
                        self._cal.cam_matrix,
                        self._cal.dist_coeffs)
                    CAi = m3d.Transform(rvecs[0][0], tvecs[0][0])
                    # OpenCV has the corner list travers in clock-wise
                    # order around the tag, starting from the top left
                    # corner. The marker position in the centre and
                    # the marker orientation with x-direction pointing
                    # towards the midpoint between corners 1 and 2,
                    # and the y-direction pointing towards the
                    # midpoint between corner 0 and 1. We wish corner
                    # 0 to be the position, x-direction towards corner
                    # 3 and y-direction towards corner 1.
                    CAi.orient.rotate_zt(-np.pi/2)
                    CAi.pos -= 0.5 * tag_size * (CAi.orient.vec_x +
                                                 CAi.orient.vec_y)
                    CAis.append(CAi)
                # Identify aruco layout in camera reference
                # Stack image points
                Pcorners = np.vstack([cs[0] for cs in corner_sets])
                # print(img_points)
                # Stack corresponding 3D corners in aruco layout reference
                Acorners = np.vstack([aruco_layout[id].corners_array
                                      for id in ids])
                # Calculate transform
                ret, rvec, tvec = cv2.solvePnP(Acorners, Pcorners,
                                               self._cal.cam_matrix,
                                               self._cal.dist_coeffs)
                CA = m3d.Transform(rvec.T[0], tvec.T[0])
            else:
                # raise Exception(
                #     'ArUcoFinder: Can not 3D locate without valid calibration'
                #     + ' ("self._cal") and given tag size ("tag_size")!')
                CA = None
                CAis = len(corner_sets) * [None]
        return self.ArUcoResult(
            aimg, CA,
            [ArUcoLoc(*icp) for icp in zip(ids, corner_sets, CAis)]
        )


@dataclasses.dataclass
class ArUcoParameters:
    aruco_dict_code: int = cv2.aruco.DICT_6X6_250
    aruco_layout: ArUcoLayout = None

    def __post_init__(self):
        self.aruco_dict = cv2.aruco.Dictionary_get(self.aruco_dict_code)
