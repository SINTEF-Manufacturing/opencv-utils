# coding=utf-8

"""
"""

from __future__ import annotations

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2024"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import typing
import threading
import time
import logging
import pathlib
import builtins

import cv2

from . import parameters as oupar
from . import calibration as oucal
from .cam_spec import CamSpec, CSIndex, CSSerial, CSDevNode


class Capture(threading.Thread):

    def __init__(self,
                 cam_spec: CamSpec | str | int | pathlib.PosixPath,
                 cap_pars: typing.Union[dict, oupar.CaptureParameters] = None,
                 calibration: oucal.Calibration = None,
                 backend: int = cv2.CAP_V4L2
                 ):
        self._cam_lock = threading.RLock()
        self._cap_cond = threading.Condition()
        if type(cap_pars) is dict:
            self.cap_pars = oupar.CaptureParameters(**cap_pars)
        elif type(cap_pars) is oupar.CaptureParameters:
            self.cap_pars = cap_pars
        elif cap_pars is not None:
            raise Exception(f'Given "cap_pars" type could '
                            f'not be loaded: {type(cap_pars)}.')
        if isinstance(cam_spec, CamSpec):
            self._cam_spec = cam_spec
        else:
            match type(cam_spec):
                case builtins.int:
                    self._cam_spec = CSIndex(cam_spec)
                case builtins.str:
                    self._cam_spec = CSSerial(cam_spec)
                case pathlib.Path | pathlib.PosixPath:
                    self._cam_spec = CSDevNode(cam_spec)
        self.cal = None  # Set later to 'calibration'
        threading.Thread.__init__(
            self,
            name=f'Cap({self._cam_spec.serial})',
            daemon=True)
        self._log = logging.getLogger(self.name)
        self._vc = cv2.VideoCapture(self._cam_spec.index, backend)
        if not self._vc.isOpened():
            raise Exception(
                f'Failed to open camera with serial "{self._cam_spec.serial}".')
        self._log.info('Initializing grabber')
        self._vc.grab()
        self.__stop = False
        self._act_frame_cycle_time = 0.0
        self._mono = False
        self._cal = calibration
        self.set_props([cap_pars])
        self._running = threading.Event()
        self._running.set()

    def __del__(self):
        self._log.info('Releasing capture')
        self.stop()

    @property
    def calibration(self):
        return self._cal

    @calibration.setter
    def calibration(self, calibration):
        if calibration is None:
            # Unset the calibration
            self._cal = None
        elif calibration.cam_spec != self._cam_spec:
            raise Exception('Calibration was set with unmatching camera ID!')
        self._cal = calibration
        self.shape = self._cal.shape
        self.focus = self._cal.focus
        self.autofocus = -1
        self.shape = self._cal.shape

    # The cam_serial must be advertised as a property from the class definition
    @property
    def cam_serial(self):
        return self._cam_spec.serial

    @property
    def cam_spec(self):
        return self._cam_spec

    def get_mono(self):
        return self._mono

    def set_mono(self, mono_value):
        self._mono = mono_value

    mono = property(get_mono, set_mono)

    def get_shape(self):
        return (int(self._vc.get(cv2.CAP_PROP_FRAME_HEIGHT)),
                int(self._vc.get(cv2.CAP_PROP_FRAME_WIDTH)))

    def set_shape(self, hw_pair):
        if self._cal is None:
            h, w = hw_pair
            with self._cam_lock:
                self._vc.set(cv2.CAP_PROP_FRAME_HEIGHT, h)
                self._vc.set(cv2.CAP_PROP_FRAME_WIDTH, w)

    shape = property(get_shape, set_shape)

    def set_cap_pars(self, cap_pars: oupar.CaptureParameters):
        with self._cam_lock:
            for p in oupar.prop_names:
                if p == 'cam_serial':
                    continue
                if hasattr(self, p) and hasattr(cap_pars, p):
                    v = getattr(cap_pars, p)
                    setattr(self, p, v)

    def get_cap_pars(self) -> oupar.CaptureParameters:
        with self._cam_lock:
            par_dict = {}
            for p in oupar.prop_names:
                if hasattr(self, p):
                    par_dict[p] = getattr(self, p)
            cap_pars = oupar.CaptureParameters(**par_dict)
            return cap_pars

    cap_pars = property(get_cap_pars, set_cap_pars)

    def set_props(self, prop_collections: list) -> list[str]:
        """Update properties from the 'prop_collections' list where first
        match takes precedence.
        """
        updated_props = []
        with self._cam_lock:
            for p in oupar.prop_names:
                v_cur = getattr(self, p)
                for pc in prop_collections:
                    if pc is not None:
                        if hasattr(pc, p):
                            v_new = getattr(pc, p)
                            if v_new is not None and v_new != v_cur:
                                updated_props.append(p)
                                setattr(self, p, v_new)
                                break
        return updated_props

    def capture(self, undistort=False, fresh=True):
        if not self.is_alive():
            if self.__stop:
                self._log.info('Reading after stopped.')
            else:
                self._log.info('Reading before started.')
            r, f = self._vc.read()
        else:
            if fresh:
                with self._cap_cond:
                    self._cap_cond.wait()
            with self._cam_lock:
                r, f = self._vc.retrieve()
        if f is None:
            return None
        if undistort and self._cal is not None:
            f = cv2.undistort(f, self._cal.cam_matrix, self._cal.dist_coeffs)
        if self._mono:
            return cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
        else:
            return f

    @property
    def running(self):
        return self._running.is_set()

    def pause(self):
        self._running.clear()

    def resume(self):
        self._running.set()

    def stop(self, join=True):
        self._log.info('Stopping')
        self.__stop = True
        if not self._running.is_set():
            self._log.info('Stopping from paused state.')
            self.resume()
        self._log.info('Releasing capture condition waiters.')
        with self._cap_cond:
            self._cap_cond.notify_all()
        if join:
            self._log.info('Waiting for thread to end.')
            self.join()
        self._log.info('Releasing video capture.')
        self._vc.release()
        time.sleep(0.5)

    def run(self):
        self._running.set()
        t_grab = t_grab_prev = time.time()
        while not self.__stop:
            self._running.wait()
            if self.__stop:
                self._log.info('Breaking from paused.')
                break
            with self._cam_lock:
                if self._vc is None:
                    self.pause()
                    continue
                self._vc.grab()
            with self._cap_cond:
                self._cap_cond.notify_all()
            t_grab_prev = t_grab
            t_grab = time.time()
            self._act_frame_cycle_time = 0.5 * (
                self._act_frame_cycle_time + t_grab - t_grab_prev)
            time.sleep(0.01)
        self._log.info('Thread ended.')


class _CapProp:
    def __init__(self, prop_name, cal_fixed=False, factor=1.0):
        self._fac = factor
        self._cal_fixed = cal_fixed
        self._pn = prop_name.upper()
        self._cv_prop = getattr(cv2, 'CAP_PROP_' + self._pn)

    def __get__(self, cap, cap_cls):
        return cap._vc.get(self._cv_prop) / self._fac

    def __set__(self, cap, value):  # prop, cap, cap_cls, value):
        if not self._cal_fixed or cap._cal is None:
            with cap._cam_lock:
                try:
                    cap._vc.set(self._cv_prop, self._fac * value)
                except TypeError:
                    cap._log.Exception(f'Setting value "{value}" on "{self._pn}":')


for p in oupar.prop_descrs:
    if not hasattr(Capture, p.name):
        setattr(Capture, p.name, _CapProp(p.name,
                                          cal_fixed=p.cal_fixed,
                                          factor=p.factor))
