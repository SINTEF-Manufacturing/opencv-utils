# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import os
import re
import pickle
import dataclasses
import pathlib
import typing
import logging

import numpy as np
import cv2
import math3d as m3d

from . import capture as oucap
from . import parameters as oupar


@dataclasses.dataclass
class CalCBResult:
    ret: float
    mtx: np.ndarray
    dist: np.ndarray
    rvec_map: dict[int, np.ndarray]
    tvec_map: dict[int, np.ndarray]

    @property
    def COs(self) -> dict[int, m3d.Transform]:
        """Get an index-based mapping of the (chessboard) object poses in
        camera reference."""
        return {idx: m3d.Transform(m3d.RotationVector(self.rvec_map[idx].T[0]),
                                   m3d.PositionVector(self.tvec_map[idx].T[0]))
                for idx in self.tvec_map.keys()}


class Calibrator:
    """Class for performing capture and calibration and storage of results
    of calibration."""

    cal_img_fn_tmpl = 'cal_img_{:03d}.bmp'
    cal_img_fn_re = re.compile('cal_img_(?P<idx>[0-9]{3}).bmp')
    BF_fn_tmpl = 'BF_{:03d}.pickle'
    BF_fn_re = re.compile('BF_(?P<idx>[0-9]{3}).pickle')
    CO_fn_tmpl = 'CO_{:03d}.pickle'
    CO_fn_re = re.compile('CO_(?P<idx>[0-9]{3}).pickle')

    def __init__(self, cal_id: pathlib.Path,
                 cal_pars: oupar.CalibrationParameters = None,
                 cap: 'oucap.Capture' = None,
                 cap_pars: oupar.CaptureParameters = None):
        """'cal_pars': Parameters for use in calibration. If not given, these
        must be found in the loaded Calibration object in the project
        denoted by 'cal_id'. 'cap': A Capture object, which holds a
        full set of fall-back parameters to those described in
        'cap_pars', and is further used for capturing calibration
        images. 'cap_pars': Specification of parameters for use in
        capturing images and fixing capture parameters with the
        Calibration object.
        """
        self._log = logging.getLogger('Calibrator')
        # self._log.setLevel(logging.INFO)
        self.cal_id = cal_id  # cal_pars.get('cal_id', None)
        self.cal_dir = pathlib.Path(self.cal_id)
        if not self.cal_dir.exists():
            self._log.info(f'Creating cal_dir: "{self.cal_dir}"')
            self.cal_dir.mkdir()
        self.cal = Calibration(self.cal_id)
        self.cap_pars = cap_pars
        self.cal_pars = cal_pars
        self.cap = cap
        # Determine camera specification
        self.cam_spec = None
        if cal_pars is not None and cal_pars.cam_spec is not None:
            cam_spec = cal_pars.cam_spec
            self._log.info(f'Using camera: "{cam_spec}"')
            # Ensure consistency
            if self.cap is not None and self.cap.cam_spec != cam_spec:
                raise Exception(
                    'Calibrator.__init__: "cap" and "cal_pars.cam_spec"' +
                    'both given and conflicting!')
            if(cap_pars is not None and
               hasattr(cap_pars, 'cam_spec') and
               cap_pars.cam_spec is not None and
               cap_pars.cam_spec != cal_pars.cam_spec):
                raise Exception(
                    'Calibrator.__init__: "cap_pars.cam_spec" and' +
                    ' "cal_pars.cam_spec"' +
                    ' both given and conflicting!')
        elif self.cap is not None:
            cal_pars.cam_spec = cap.cam_spec
        elif self.cal.cam_spec is not None:
            cal_pars.cam_spec = self.cal.cam_spec
        else:
            raise Exception(
                'Calibrator.__init__: "cam_id"' +
                ' could not be determined')
        # Set capture properties if a capture was given
        if self.cap is not None:
            self._log.info('Setting capture parameters on camera.')
            self.cap.set_props([cal_pars, cap_pars])
        # Set calibration properties
        updated_props = self.cal.set_props([cal_pars, cap_pars, cap])
        # Invalidate calibration images, if changes
        if 'cb_shape' in updated_props or 'cb_square_size' in updated_props:
            self._log.info('Changed chessboard parameters. Clearing current calibration')
            self.clear()
        # Check that necessary calibration parameters are present
        if self.cal.cb_shape is None or self.cal.cb_square_size is None:
            raise Exception('Calibration.__init__: Need "cb_shape" and'
                            + ' "cb_square_size" in calibration!')
        # Save current state of the calibration
        self.cal.save()
        # Read calibration images
        self._load_cal_imgs()

    def _get_cal_img_fns(self) -> dict[int, str]:
        """Return a map of all stored calibration image file
        names."""
        cal_img_fns = {}
        for fn in self.cal_dir.iterdir():
            m = self.cal_img_fn_re.search(str(fn))
            if m is not None:
                idx = int(m.group('idx'))
                cal_img_fns[idx] = self.cal_img_fn_tmpl.format(idx)
        return cal_img_fns

    @property
    def n_cal_imgs(self) -> int:
        return len(self.cal_imgs)

    @property
    def max_index(self) -> int:
        if len(self.cal_imgs) == 0:
            return -1
        return max(self.cal_imgs.keys())

    def _load_cal_imgs(self):
        self.cal_imgs = {}
        self.cal_img_fns = self._get_cal_img_fns()
        for idx, cifn in self.cal_img_fns.items():
            self.cal_imgs[idx] = cv2.imread(str(self.cal_dir / cifn),
                                            cv2.IMREAD_GRAYSCALE)
        self._log.info(f'Loaded {self.n_cal_imgs} calibration images.')


    def _get_BF_fns(self) -> dict[int, str]:
        """Return a map of all stored camera pose file
        names."""
        BF_fns = {}
        for fn in self.cal_dir.iterdir():
            m = self.BF_fn_re.search(str(fn))
            if m is not None:
                idx = int(m.group('idx'))
                BF_fns[idx] = self.BF_fn_tmpl.format(idx)
        return BF_fns

    def get_BFs(self) -> dict[int, m3d.Transform]:
        BFs = {}
        BF_fns = self._get_BF_fns()
        for idx, BFfn in BF_fns.items():
            with (self.cal_dir / BFfn).open('rb') as f:
                BFs[idx] = pickle.load(f)
        return BFs

    def _get_CO_fns(self) -> dict[int, str]:
        """Return a map of all stored camera pose file
        names."""
        CO_fns = {}
        for fn in self.cal_dir.iterdir():
            m = self.CO_fn_re.search(str(fn))
            if m is not None:
                idx = int(m.group('idx'))
                CO_fns[idx] = self.CO_fn_tmpl.format(idx)
        return CO_fns

    def get_COs(self) -> dict[int, m3d.Transform]:
        COs = {}
        CO_fns = self._get_CO_fns()
        for idx, COfn in CO_fns.items():
            with (self.cal_dir / COfn).open('rb') as f:
                COs[idx] = pickle.load(f)
        return COs

    def _clear_data(self):
        self.cal_imgs = {}
        self.cal_img_fns = {}
        for cifn in self._get_cal_img_fns().values():
            (self.cal_dir / cifn).unlink()
        for BFfn in self._get_BF_fns().values():
            (self.cal_dir / BFfn).unlink()
        for COfn in self._get_CO_fns().values():
            (self.cal_dir / COfn).unlink()

    def clear(self):
        self._clear_data()
        self.cal.clear()

    def cap_cal_imgs(self, n=10, append=True):
        if self.cap is None:
            try:
                self.cap = oucap.Capture(self.cal.cam_id)
            except Exception as e:
                raise Exception('Calibrator.cap_cal_imgs: ' +
                                'Failed to create capture on ' +
                                f'cam_id "{self.cal.cam_id}".' +
                                f'Exception:\n{e}')
        if append:
            self._load_cal_imgs()
            ci = self.max_index + 1
        else:
            self.clear()
            ci = 0
        while len(self.cal_imgs) < n:
            # input('Pose chessboard')
            c = self.cap.capture()
            cv2.imshow('capture calibration images', c)
            key = cv2.waitKey(1)
            if key != -1:
                self._log.info('Trying to add cal image {}'.format(ci))
                if self.add_cal_img(c):
                    ci += 1
                    self._log.info('... Success')
                else:
                    self._log.warning('Chessboard not found or wrong image format')
        cv2.destroyWindow('capture calibration images')

    def add_cal_img(self,
                    img: np.ndarray,
                    BF: m3d.Transform = None,
                    show=False) -> bool:
        """Add the given image, if it has correct format and if the configured
        chessboard can be found. If 'BF' is not None, it is stored as
        the robot flange pose in robot base reference.
        """
        if img.shape != self.cap_pars.shape:
            return False
        ret, cbcs = cv2.findChessboardCorners(img, self.cal.cb_shape)
        if not ret:
            if show:
                wnd_tit = 'CB NOT found'
                cv2.imshow(wnd_tit, img)
                cv2.waitKey(-1)
                cv2.destroyWindow(wnd_tit)
                cv2.waitKey(1)
            return False
        else:
            idx = self.max_index + 1
            cv2.imwrite(
                str(self.cal_dir /
                    self.cal_img_fn_tmpl.format(idx)),
                img)
            self.cal_imgs[idx] = img.copy()
            if BF is not None:
                with (self.cal_dir /
                      self.BF_fn_tmpl.format(idx)).open('wb') as f:
                    pickle.dump(BF, f)
            if show:
                imgcb = cv2.drawChessboardCorners(
                    img, self.cal.cb_shape, cbcs, ret)
                wnd_tit = f'CB found {idx}'
                cv2.imshow(wnd_tit, imgcb)
                cv2.waitKey(-1)
                cv2.destroyWindow(wnd_tit)
                cv2.waitKey(1)
            return True

    def _cal_cbs(self,
                 criteria=(cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT,
                           300, 0.0001),
                 sub_pix=True,
                 estimate_errors=True) -> None:
        obj_pt_sets = []
        obj_pts = self.cal.cb_obj_points.astype(np.float32)
        self._log.debug(f'obj_pts={obj_pts}')
        img_pt_sets = []
        i2idx = list(self.cal_imgs.keys())
        for i, idx in enumerate(i2idx):
            img = self.cal_imgs[idx]
            ret, img_pts = cv2.findChessboardCorners(
                img, self.cal.cb_shape)
            self._log.debug(f'ret={ret}, img_pts={img_pts}')
            if not ret:
                self._log.warning(f'Skipping image index {idx}. '
                                  'CB corners not found!')
                continue
            obj_pt_sets.append(obj_pts)
            if not sub_pix:
                img_pt_sets.append(img_pts.squeeze(1))
            else:
                # Refine image points
                cb_img_sub_pts = cv2.cornerSubPix(img, img_pts,
                                                  (5, 5), (-1, -1), criteria)
                img_pt_sets.append(cb_img_sub_pts)
        # obj_pt_sets = np.vstack(obj_pt_sets)
        # img_pt_sets = np.vstack(img_pt_sets)
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(obj_pt_sets,
                                                           img_pt_sets,
                                                           img.shape[::-1],
                                                           None, None)
        # Cast rvecs and tvecs to index maps
        if estimate_errors:
            pass
            # from .calibration_error_estimation import CalibrationErrorEstimator
            # cee = CalibrationErrorEstimator( ... )
            # ...
        # self._log.debug([type(e) for e in (ret, mtx, dist, rvecs[0], tvecs)])
        tvec_map = {i2idx[i]: tvecs[i] for i in range(len(tvecs))}
        rvec_map = {i2idx[i]: rvecs[i] for i in range(len(rvecs))}
        self._cal_cb_res = CalCBResult(ret, mtx, dist, rvec_map, tvec_map)

    def calibrate(self, save_cb_poses=True) -> None:
        self._cal_cbs()
        self.cal.cam_matrix = self._cal_cb_res.mtx
        self.cal.dist_coeffs = self._cal_cb_res.dist
        self.cal.save()
        if save_cb_poses:
            # Save the chessboard (O) in camera (C) reference
            for idx, COi in self._cal_cb_res.COs.items():
                with (self.cal_dir / self.CO_fn_tmpl.format(idx)).open('wb') as f:
                    pickle.dump(COi, f)


@dataclasses.dataclass
class Calibration:
    """Class for restoring and holding the parameters for and result of a
    calibration."""

    # Constants
    cal_fn = pathlib.Path('cal.pickle')

    # Annotated properties
    cal_id: typing.Union[str, pathlib.Path]
    cam_spec: str = None
    cb_square_size: float = None
    cb_shape: tuple[int, int] = None
    shape: tuple[int, int] = None
    focus: int = None
    autofocus: bool = None
    zoom: int = None
    dist_coeffs: typing.List[float] = None
    cam_matrix: list[list[float]] = None
    error_linarity: float = None  # Average error in corners to best fittet line
    error_reproj: float = None  # Average error in re-projected corner pixels

    def __post_init__(self):
        self.cal_id = pathlib.Path(self.cal_id).expanduser()
        self.cal_path = self.cal_id / self.cal_fn
        self.load()

    @property
    def is_valid(self):
        return self.cam_matrix is not None and self.dist_coeffs is not None

    @property
    def cb_obj_points(self):
        """Return an Nx3-array of row vectors for the calibration chessboard
        inner corner positions in chessboard reference, and in the
        correct order for matching the corners found in the image.
        """
        obj_points = np.zeros((self.cb_shape[0] * self.cb_shape[1], 3))
        obj_points[:, :2] = self.cb_square_size * np.mgrid[
            0:self.cb_shape[0],
            0:self.cb_shape[1]].T.reshape(-1, 2)
        return obj_points

    def set_props(self, prop_collections: typing.List) -> typing.List[str]:
        """Update properties from the 'prop_collections' list where first
        match takes precedence.
        """
        updated_props = []
        for p in self.__annotations__.keys():
            v_cur = getattr(self, p)
            for pc in prop_collections:
                if pc is not None:
                    if hasattr(pc, p):
                        v_new = getattr(pc, p)
                        if v_new is not None and v_new != v_cur:
                            updated_props.append(p)
                            setattr(self, p, v_new)
                            break
        if len(updated_props) > 0:
            # If any property changed, the calibration is invalid.
            self.clear()
        return updated_props

    def load(self):
        if self.cal_path.is_file():
            for (p, v) in pickle.load(self.cal_path.open('rb')).items():
                setattr(self, p, v)

    def save(self):
        pars = dict([(p, getattr(self, p)) for p in self.__annotations__])
        pickle.dump(pars, self.cal_path.open('wb'))

    # @property
    # def cam_matrix(self):
    #     return np.array(self.cal_pars['cam_matrix'])

    # @property
    # def dist_coeffs(self):
    #     return np.array(self.cal_pars['dist_coeffs'])

    # def get_cal_pars(self):
    #     return dict([(p, getattr(self, p)) for p in self.par_names])

    # def set_cal_pars(self, **cal_par_dict):
    #     for p in self.par_names:
    #         setattr(self, p, cal_par_dict[p])
    #     self.save_cal()

    # cal_pars = property(get_cal_pars, set_cal_pars)

    def clear(self):
        self.dist_coeffs = None
        self.cam_matrix = None
        self.save()
