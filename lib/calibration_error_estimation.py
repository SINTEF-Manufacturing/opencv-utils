#--------------------------------------------------------------------
#Module Description
#--------------------------------------------------------------------
"""
 Calibrator error class
"""
#--------------------------------------------------------------------
#Administration Details
#--------------------------------------------------------------------
__author__ = "Mats Larsen"
__credits__ = ["Mats Larsen"]
__copyright__ = "Sintef Raufoss Manufacturing 2019"
__license__ = "SiMa"
__maintainer__ = "Mats Larsen"
__email__ = "Mats.Larsen@sintef.no"
__status__ = "Development"
__date__ = "28092019"
__version__ = "0.1"
#--------------------------------------------------------------------
#Import
#--------------------------------------------------------------------
import dataclasses
import math

import math2d.geometry as m2dg
import cv2
import numpy as np


@dataclasses.dataclass
class CalibrationErrorEstimator:
    '''
    Class for computing error
    '''
    mtx : np.ndarray
    dist : np.ndarray
    rvecs : np.ndarray
    tvecs : np.ndarray
    objpoints : np.ndarray
    imgpoints : np.ndarray
    cb_shape   : np.ndarray

    @classmethod
    def checkBorder(cls,corners,w,h):
        '''
        '''
        # If any corners are within BORDER pixels of the screen edge, reject the detection by setting ok to false
        # NOTE: This may cause problems with very low-resolution cameras, where 8 pixels is a non-negligible fraction
        # of the image size. See http://answers.ros.org/question/3155/how-can-i-calibrate-low-resolution-cameras
        #print(ok,corners)
        BORDER = 8
        c = np.float32([j[0] for j in corners])
        if not all([(BORDER < x < (w - BORDER)) and (BORDER < y < (h - BORDER)) for (x, y) in c]):
             return False
        else:
            return True

    def refine_corners(self,img,corners,criteria):
        '''
        '''
        def _pdist(p1, p2):
            """
            Distance bwt two points. p1 = (x, y), p2 = (x, y)
            """
            #print(p1,p2)
            return math.sqrt(math.pow(p1[0] - p2[0], 2) + math.pow(p1[1]- p2[1], 2))
        # Use a radius of 1/2 the minimum distance between corners. This should be large enough to snap to the
        # correct corner, but not so large as to include a wrong corner in the search window.
        rows =  min(self.cb_shape[0], self.cb_shape[1])
        cols = max(self.cb_shape[0], self.cb_shape[1])
        min_distance = float("inf")
        for row in range((rows)):
            for col in range(cols - 1):
                index = row*rows + col
                min_distance = min(min_distance, _pdist(corners[index][0], corners[index + 1][0]))

        radius = int(math.ceil(min_distance * 0.5))

        return cv2.cornerSubPix(img, corners, (radius,radius), (-1,-1),criteria)

    @classmethod
    def get_corners(cls,img,cb_shape,refine=True,criteria=(cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT,
              300, 0.0001),plot=False):
        '''
        '''
        # Find the chess board corners
        settings = cv2.CALIB_CB_ADAPTIVE_THRESH | cv2.CALIB_CB_NORMALIZE_IMAGE
        (ret, corners) = cv2.findChessboardCorners(img, cb_shape,flags=settings)
        # If found, add object points, image points (after refining them)
        if ret == True:
            #corners = np.float32([j[0] for j in corners])
            ret = CalibrationError.checkBorder(corners,img.shape[1::-1][0],img.shape[1::-1][1])
            if ret:
                if refine:
                    corners = CalibrationError.refine_corners(img,corners,cb_shape,criteria)

                # Draw and display the corners
                if plot:
                    gray = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
                    gray = cv2.drawChessboardCorners(gray, cb_shape, corners,ret)
                    cv2.imshow('img',gray)
                    cv2.waitKey(1000)
                    cv2.destroyAllWindows()
                return True, corners
            else:
                return False, None
        return False,None

    def reprojection_error(self):
        '''
        Compute the absolute norm between what we got with our transformation and the corner finding algorithm.
        This should be as close to zero as possible.
        To find the average error we calculate the arithmetical mean of the errors calculate for all the calibration images.
        '''
        mean_error = 0
        for i in range(len(self.rvecs)):
            # Project points from 3D chessboard points to the image plane 2D
            imgpoints2, _ = cv2.projectPoints(self.objpoints,        # 3D points
                                              self.rvecs[i],    # Rotation vector
                                              self.tvecs[i],    # Translation vector
                                              self.mtx,         # Camera matrix
                                              self.dist)  # Distortion vector
            #Compute the error as a loss function norm L2 or  least squares error
            error = cv2.norm(self.imgpoints[i],imgpoints2, cv2.NORM_L2)/len(imgpoints2)
            mean_error += error
        error = mean_error/len(self.rvecs)
        return error

    def _get_outside_corners(self, corners):
        """
        Return the four corners of the board as a whole, as (up_left, up_right, down_right, down_left).
        Params:
        - corners -> np.array : Position of corners
        """
        xdim = self.cb_shape[0]
        ydim = self.cb_shape[1]

        if len(corners) != xdim * ydim:
            raise Exception("Invalid number of corners! %d corners. X: %d, Y: %d" % (len(corners),
                                                                                     xdim, ydim))

        up_left    = np.array( corners[0] )
        up_right   = np.array( corners[xdim - 1] )
        down_right = np.array( corners[-1] )
        down_left  = np.array( corners[-xdim] )

        return (up_left, up_right, down_right, down_left)

    def linear_error(self,images):
        """
        Detect the checkerboard and compute the linear error.
        Mainly for use in tests.
        """
        errors = []
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT,
                  300, 0.0001)
        for img in images:
            undist = cv2.undistort(img, self.mtx, self.dist, None, self.mtx)
            ret, p = CalibrationError.get_corners(undist,self.cb_shape,criteria=criteria,plot=False)
            errors += self.get_linear_error(np.squeeze(p)) # Linear error in meter, self.linear_error(undistort_corners)
        return np.mean(errors),np.std(errors)

    def get_linear_error(self, corners):
        """
        Returns the linear error for a set of corners detected in the unrectified image.
        """
        cc = self.cb_shape[0]
        cr = self.cb_shape[1]
        errors = []

        for r in range(cr):
            x = corners[(cc * r) + 0:(cc * r) + cc - 1,0]
            y = corners[(cc * r) + 0:(cc * r) + cc - 1,1]
            points = np.array([x,y]).T
            line = m2dg.Line.new_fitted_points(points)

            for p in points:
                errors.append(line.dist(p))
        return errors

    def get_area(self,images):
        """
        Get 2d image area of the detected checkerboard.
        The projected checkerboard is assumed to be a convex quadrilateral, and the area computed as
        |p X q|/2; see http://mathworld.wolfram.com/Quadrilateral.html.
        """
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT,
                  300, 0.0001)
        t_area = []
        for img in images:
            undist = cv2.undistort(img, self.mtx, self.dist, None, self.mtx)
            ret1, un_p = CalibrationError.get_corners(img,self.cb_shape,criteria=criteria,plot=False)
            ret2, p = CalibrationError.get_corners(undist,self.cb_shape,criteria=criteria,plot=False)

            if ret1 and ret2:

                areas = []
                for i in [np.squeeze(p),np.squeeze(un_p)]:
                    (up_left, up_right, down_right, down_left) = self._get_outside_corners(i)

                    a = up_right - up_left
                    b = down_right - up_right
                    c = down_left - down_right
                    p = b + c
                    q = a + b
                    areas.append(abs(p[0]*q[1] - p[1]*q[0]) / 2.)
                t_area.append(areas)
        t_area = np.mean(t_area,axis=0)

        return 1 -t_area[1] / t_area[0]

    def get_skew(self,images):
        """
        Get skew for given checkerboard detection.
        Skew is proportional to the divergence of three outside corners from 90 degrees.
        Params:
        Returns:
        """
        def angle(a, b, c):
            """
            Return angle between lines ab, bc
            """
            ab = a - b
            cb = c - b
            return math.acos(np.dot(ab,cb) / (np.linalg.norm(ab) * np.linalg.norm(cb)))

        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT,
                  300, 0.0001)
        skews = []
        for img in images:
            undist = cv2.undistort(img, self.mtx, self.dist, None, self.mtx)
            ret, un_p = CalibrationError.get_corners(undist,self.cb_shape,criteria=criteria,plot=False)
            if ret:
                up_left, up_right, down_right, _ = self._get_outside_corners(un_p)
                skews.append(abs(90.0 - np.rad2deg(angle(up_left[0], up_right[0], down_right[0]))))
            else:
                continue

        return np.mean(skews), np.std(skews)
