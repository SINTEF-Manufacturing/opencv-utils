# coding=utf-8
from __future__ import annotations

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2024"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import collections
import dataclasses
import typing
import copy
import pathlib
import pickle

import math3d as m3d
import cv2

from .cam_spec import CamSpec

#
# OU Capture properties
#

@dataclasses.dataclass
class PropDescr:
    name: str
    type: type
    cal_fixed: bool
    instant: bool = False
    factor: float = 1.0

    def __post_init__(self):
        self._tuple = (self.name, self.type, self.cal_fixed, self.instant, self.factor)

    @property
    def tuple(self) -> tuple:
        return self._tuple

    # def __getitem__(self, index: int):
    #     return self._tuple[index]



prop_descrs = tuple([
    PropDescr('aperture', int, False, True),
    PropDescr('auto_exposure', int, False, False),  # 1: manual, 3: auto
    PropDescr('auto_wb', bool, False, False),
    PropDescr('brightness', int, False, True),
    PropDescr('contrast', int, False, True),
    PropDescr('exposure', int, False, True),
    PropDescr('fps', int, False, False),
    PropDescr('gain', int, False, True),
    PropDescr('gamma', int, False, True),
    PropDescr('hue', int, False, True),
    PropDescr('saturation', int, False, True),
    PropDescr('sharpness', int, False, True),
    # PropDescr('temperature', int, False, True),
    PropDescr('wb_temperature', int, False, True),
    PropDescr('mono', bool, False, True),
    PropDescr('cam_serial', str, True, False),
    PropDescr('autofocus', bool, True, False),
    PropDescr('focus', int, True, True),
    PropDescr('zoom', int, True, True),
    PropDescr('tilt', int, True, True),
    PropDescr('pan', int, True, True),
    PropDescr('shape', tuple[int, int], True, False),
])

prop_names = [pd.name for pd in prop_descrs]
prop_map = {pd.name: pd for pd in prop_descrs}
instant_props = tuple([pd for pd in prop_descrs if pd.instant])
instant_prop_names = tuple([pd.name for pd in instant_props])
prop_tuples = tuple([pd._tuple for pd in prop_descrs])

CaptureParameters = collections.namedtuple(
    'CaptureParameters',
    prop_names,
    defaults=len(prop_descrs)*[None]
)

CaptureParameters.copy = lambda self: copy.deepcopy(self)


# @dataclasses.dataclass
# class CaptureParameters:
#     aperture: int = None
#     auto_exposure: int = None  # 1: manual, 3: auto
#     auto_wb: bool = None
#     brightness: int = None
#     contrast: int = None
#     exposure: int = None
#     fps: int = None
#     gain: int = None
#     gamma: int = None
#     hue: int = None
#     saturation: int = None
#     sharpness: int = None
#     # temperature: int = None
#     wb_temperature: int = None
#     mono: bool = None
#     cam_serial: str = None
#     autofocus: bool = None
#     focus: int = None
#     zoom: int = None  # , 100.0),
#     tilt: int = None  # , 3600.0),
#     pan: int = None  # , 3600.0),
#     shape: tuple[int, int] = None

#     def copy(self):
#         return copy.deepcopy(self)

#     @classmethod
#     def load(self, cpf: pathlib.Path | str
#              ) -> CaptureParameters | None:
#         if isinstance(cpf, str):
#             cpf = pathlib.Path(cpf)
#         if not cpf.is_file():
#             cp = None
#             print(f'No such file: "{cpf}"')
#         else:
#             try:
#                 cp = pickle.load(cpf.open('rb'))
#                 cpf.close()
#             except:
#                 print(f'Could not unpickle "{cpf}"')
#                 cp = None
#         if not isinstance(cp, CaptureParameters):
#             print(f'Object found in "{cpf}" did not unpickle to CaptureParameters')
#         return cp

#     def save(self, cpf: pathlib.Path | str):
#         if isinstance(cpf, str):
#             cpf = pathlib.Path(cpf)
#             pickle.dump(self, cpf.open('wb'))


@dataclasses.dataclass
class CalibrationParameters:
    cb_shape: typing.Tuple[int, int] = None
    cb_square_size: float = None
    cam_spec: CamSpec = None

    @classmethod
    def load(self, cpf: pathlib.Path | str
             ) -> CalibrationParameters | None:
        if isinstance(cpf, str):
            cpf = pathlib.Path(cpf)
        if not cpf.is_file():
            cp = None
            print(f'No such file: "{cpf}"')
        else:
            try:
                cp = pickle.load(cpf.open('rb'))
                cpf.close()
            except:
                print(f'Could not unpickle "{cpf}"')
                cp = None
        if not isinstance(cp, CalibrationParameters):
            print(f'Object found in "{cpf}" did not unpickle to CalibrationParameters')
        return cp

    def save(self, cpf: pathlib.Path | str):
        if isinstance(cpf, str):
            cpf = pathlib.Path(cpf)
            pickle.dump(self, cpf.open('wb'))
