# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import pathlib
import runpy


class Config:
    """Dummy class for holding config namespace."""
    def __init__(self, cdict):
        self.__dict__.update(cdict)


# Default config files listed by precedense
_default_configs: tuple[pathlib.Path] = (
    pathlib.Path.cwd() / 'ou_config.py',
    pathlib.Path.cwd() / 'config.py')


def load_config(config_file=None) -> Config | None:
    if config_file is None:
        # Pick the the first default config which exists
        for dcfg in _default_configs:
            if dcfg.is_file():
                config_file = dcfg
                break
    if config_file is not None:
            # raise Exception(
            #     'ou.load_config: Error: No config file given, and ' +
            #     'no default alternative exists!')
        return Config(runpy.run_path(config_file))
    else:
        return None
