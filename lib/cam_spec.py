# coding=utf-8

"""
"""

from __future__ import annotations

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import pathlib
import dataclasses

import pyudev as udev


class CamSpec:

    @property
    def dev(self) -> udev.Device:
        raise NotImplementedError('The "dev" property must be implemented in a base class!')

    @property
    def dev_node(self) -> pathlib.Path:
        # raise NotImplementedError()
        return pathlib.Path(self.dev.device_node)

    @property
    def model(self) -> str:
        return self.dev.properties['ID_MODEL']

    @property
    def serial(self) -> str:
        # raise NotImplementedError()
        return self.dev.properties['ID_SERIAL_SHORT']

    @property
    def index(self) -> int:
        # raise NotImplementedError()
        return int(self.dev.sys_number)

    @classmethod
    def _serial_from_dev(cls, dev: udev.Device) -> str | None:
        if 'ID_SERIAL_SHORT' in dev.properties:
            return dev.properties['ID_SERIAL_SHORT']
        else:
            return ''

    def __eq__(self, other):
        return self.serial == other.serial

    @classmethod
    def get_available_cam_serials(cls) -> dict[str, str]:
        """Get a map of short id (ID_SERIAL_SHORT) to camera model name for
        all capture capable camera. This is useful for selecting
        cameras to use.
        """
        ctx = udev.Context()
        cam_map = {}
        for p in pathlib.Path('/dev').glob('video*'):
            d = udev.Device.from_device_file(ctx, p)
            if(d.properties['ID_V4L_CAPABILITIES'] == ':capture:' and
               cls._serial_from_dev(d) is not None):
                cam_map[cls._serial_from_dev(d)] = d.properties['ID_MODEL']
        return cam_map

    @classmethod
    def get_available_cam_devices(cls) -> list[udev.device._device.Device]:
        """Get a map of capture capable camera devices identifying possible camera
        specifications.
        """
        ctx = udev.Context()
        cam_devs = []
        for p in pathlib.Path('/dev').glob('video*'):
            d = udev.Device.from_device_file(ctx, p)
            if d.properties['ID_V4L_CAPABILITIES'] == ':capture:':
                cam_devs.append(d)
        return cam_devs


@dataclasses.dataclass
class CSSerial(CamSpec):
    _serial: str

    @property
    def dev(self) -> udev.Device | None:
        ctx = udev.Context()
        for p in pathlib.Path('/dev').glob('video*'):
            d = udev.Device.from_device_file(ctx, p)
            if(self._serial_from_dev(d) == self._serial and
               d.properties['ID_V4L_CAPABILITIES'] == ':capture:'):
                return d
        return None

    @property
    def serial(self) -> str:
        return self._serial


@dataclasses.dataclass
class CSIndex(CamSpec):
    _index: int

    @property
    def dev(self) -> udev.Device | None:
        ctx = udev.Context()
        for p in pathlib.Path('/dev').glob('video*'):
            d = udev.Device.from_device_file(ctx, p)
            if int(d.sys_number) == self._index:
                return d
        return None

    @property
    def index(self):
        return self._index


@dataclasses.dataclass
class CSDevNode(CamSpec):
    _dev_node: pathlib.Path

    @property
    def dev(self) -> udev.Device:
        ctx = udev.Context()
        d = udev.Device.from_device_file(ctx, self._dev_node)
        return d
