# coding=utf-8

"""Analysis module using a calibration for 3D processing of
corresponding images. The main functionality is to find 3D lines in
camera reference corresponding to distorted or undistorted pixel
coordinates.

Pixel coordinates:
distorted: u,v
undistorted: u',v'
u,v - undistort -> u',v'

Image plane coordinates in camera reference: X, Y, given Z


Camera coordinates in camera reference: x, y, z

"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Mats Larsen"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import typing

import numpy as np
import math3d.geometry as m3dg
import cv2

from . import calibration


class RayGetter:

    def __init__(self, cal_id=None, cal=None):
        if cal is not None:
            self._cal = cal
        elif cal_id is not None:
            self._cal = calibration.Calibration(cal_id=cal_id)
        else:
            raise Exception(
                'Image3DAnalyser: Must have "cal_id" or "calibration"')

    def undistort(self,
                  pixc_dist: np.ndarray  # Nx2 pixel coordinates in
                                         # image array
                  ) -> np.ndarray:  # Nx2 image plane coordinates in
                                    # camera reference
        return cv2.undistortPoints(
            np.array(pixc_dist, dtype=np.float32)[:, ::-1],
            self._cal.cam_matrix, self._cal.dist_coeffs).reshape(-1, 2)

    def get_ray_undistorted(self,
                            image_points: np.ndarray  # Nx2 image
                                                      # points in
                                                      # camera
                                                      # coordinates
                            ) -> list[m3dg.Line]:
        return [m3dg.Line(point=[0, 0, 0], direction=list(ip)+[1])
                for ip in image_points]

    def get_ray_distorted(self,
                          pixc_dist: np.ndarray  # Nx2 pixel
                                                 # coordinates in
                                                 # image array
                          ) -> list[m3dg.Line]:
        return self.get_ray_undistorted(self.undistort(pixc_dist))
