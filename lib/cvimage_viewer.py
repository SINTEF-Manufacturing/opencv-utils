# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2024"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import logging

import cv2


class CVImageViewer(threading.Thread):

    def __init__(self, window_name=None, mouse_events=True):
        self._wn = window_name
        if self._wn is None:
            self._wn = f'ImgView<{id(self)}>'
        threading.Thread.__init__(self,
                                  name=f'IV<{window_name}>',
                                  daemon=True)
        self._log = logging.getLogger(self.name)
        self._mevs = mouse_events
        self._mouse_clicked = threading.Condition()
        self._mouse_click_pos = None
        self._image = None
        self._image_submitted = threading.Event()
        self._stop_flag = False
        # self._close_flag = False
        self.start()

    def __del__(self):
        self.close()

    def close(self):
        # self._close_flag = True
        self.stop(join=True)

    @property
    def mouse_click_pos(self):
        return self._mouse_click_pos
    pos = mouse_click_pos

    def wait_for_mouse(self):
        with self._mouse_clicked:
            self._mouse_clicked.wait()

    def _mouse_cb(self, event, x, y, flags, userdata):
        if event & cv2.EVENT_LBUTTONDOWN:
            self._log.debug(f'Mouse ({bin(event)},{bin(flags)}) button pressed at ({x}, {y})')
            self._mouse_click_pos = (x, y)
            with self._mouse_clicked:
                self._mouse_clicked.notify_all()

    def get_image(self):
        return self._image

    def set_image(self, image):
        self._image = image.copy()
        self._image_submitted.set()

    image = property(get_image, set_image)
    
    def stop(self, join=True):
        self._stop_flag = True
        self._image_submitted.set()
        if join:
            self.join()

    def run(self):
        cv2.namedWindow(self._wn)
        if self._mevs:
            cv2.setMouseCallback(self._wn, self._mouse_cb)
        while not self._stop_flag:
            self._image_submitted.wait(0.5)
            if not self._stop_flag and self._image is not None:
                cv2.imshow(self._wn, self._image)
                self._image_submitted.clear()
                cv2.waitKey(1)
        cv2.destroyWindow(self._wn)
            
