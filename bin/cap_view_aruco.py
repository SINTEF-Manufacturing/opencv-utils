# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import argparse
import itertools
import atexit

import numpy as np
import cv2
import math3d as m3d
import math3d.geometry as m3dg

from opencv_utils import Capture  # , CaptureViewer
from opencv_utils.tools.aruco import ArUcoFinder
from opencv_utils.calibration import Calibration
from opencv_utils.config_loader import load_config


# Load default config ('config.py' in current working directory)
cfg = load_config()

ap = argparse.ArgumentParser()
ap.add_argument('--cal_id', type=str)
args = ap.parse_args()

c = Capture(cfg.cam_spec, cfg.cap_pars)
atexit.register(c.stop)
# c.start()

if args.cal_id is not None:
    cal = Calibration(args.cal_id)
else:
    cal = None

af = ArUcoFinder(cfg.aruco_pars.aruco_dict, cal)
# cv = CaptureViewer(c)
# cv._post_procs.append(app)
# cv.start()

img = c.capture()
af_res = af(img, poses_3d=True,
            aruco_layout=cfg.aruco_layout)
a_dict = {a.id: a for a in af_res.aruco_locs}

cv2.imshow('', af_res.aruco_img)
cv2.waitKey(-1)

# if None not in (a_dict[0].CAi, a_dict[1].CAi, a_dict[4].CAi):
#     print('Orientation errors [deg]:',
#           [np.rad2deg(a.CA.orient.ang_dist(b.CA.orient))
#            for (a, b) in itertools.combinations(arucos, 2)])
#     CA = m3d.Transform()
#     CA.pos = a_dict[0].CAi.pos
#     CA.orient = m3d.Orientation.new_from_xy(
#         a_dict[1].CAi.pos - a_dict[0].CAi.pos,
#         a_dict[0].CAi.pos - a_dict[4].CAi.pos)
#     AC = CA.inverse


if len(af_res.aruco_locs) >= 3 and af_res.aruco_locs[0].CAi is not None:
    # Position of centres in camera and ArUco layout coordinates
    CpaTs = np.array([a.CAi.pos.array for a in af_res.aruco_locs])
    ApaTs = np.array([cfg.aruco_layout[a.id].AAi.pos.array
                      for a in af_res.aruco_locs])

    print('Orientation errors [deg]:',
          [np.rad2deg(a.CAi.orient.ang_dist(b.CAi.orient))
           for (a, b) in itertools.combinations(af_res.aruco_locs, 2)])

    # ArUc reference in camera reference
    CA = m3d.Transform.new_from_point_sets(CpaTs, ApaTs)
    CplaneA = m3dg.Plane(pn_pair=(CA.pos, CA.orient.vec_z))
    AC = CA.inverse

    for a in a_dict.values():
        a.AAi = AC * a.CAi
        print(a.id, a.AAi.pos)
