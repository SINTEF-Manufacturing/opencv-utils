# coding=utf-8

"""
From https://www.authentise.com/post/detecting-circular-shapes-using-contours
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import sys
import atexit

import cv2
#import skimage.measure as meas
#import numpy as np
from opencv_utils import Capture, CaptureViewer
from opencv_utils.tools import EllipsesFinder
from opencv_utils.config_loader import load_config


# Load default config ('config.py' in current working directory)
cfg = load_config()

c = Capture(cfg.cam_spec, cfg.cap_pars)
# These parameters should be set on a by-camera and by-scene basis:
# c.mono = True
# c.focus = cfg.cap_pars.focus
# c.auto_exposure = 1
# c.exposure = 1000
# c.contrast = 50
# c.sharpness = 0
# c.brightness = 150
# #c.shape = (640, 480)
# c.shape = cfg.cap_pars.shape
# # c.shape = (320, 240)
c.start()

ef = EllipsesFinder(thr1=50, thr2=200, sigma=0.0)

cv = CaptureViewer(c)
cv._post_procs.append(ef)
atexit.register(cv.stop)
cv.start()
