#!/usr/bin/python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import matplotlib.pyplot as plt
import cv2


dpt = 100  # Number of dots per tile
tile_shape = np.array((8, 11))
img_shape = dpt * tile_shape
img = 255 * np.ones(img_shape, dtype=np.uint8)

for i in range(tile_shape[0]):
   for j in range(tile_shape[1]):
      if (i+j) % 2:
         # Paint it black
         i0 = i * dpt
         i1 = (i+1) * dpt
         j0 = j * dpt
         j1 = (j+1) * dpt
         img[i0:i1, j0:j1] = 0

cv2.imwrite('chessboard_{1}x{2}@{0}.bmp'.format(dpt, *tile_shape), img)
plt.imshow(img, cmap='gray')
plt.show()
