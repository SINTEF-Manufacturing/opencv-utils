# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import argparse
import pickle

import cv2
import numpy as np

import opencv_utils.capture as oucap
import opencv_utils.calibration as oucal
from opencv_utils.config_loader import load_config


ap = argparse.ArgumentParser()
ap.add_argument('cal_id', type=str)
ap.add_argument('--conf', type=str, default=None)
ap.add_argument('--cal', action='store_true')
ap.add_argument('--cap', action='store_true')
ap.add_argument('--rg', action='store_true')
ap.add_argument('--n_cal_imgs', type=int, default=0,
                help="""Sample to ensure this amount of calibration images in the image
                base.""")
args = ap.parse_args()

# Load default config ('config.py' in current working directory)
cfg = load_config(args.conf)

if args.cap or args.n_cal_imgs > 0:
    cap = oucap.Capture(cfg.cam_spec, cfg.cap_pars)
    # cap.start()
else:
    cap = None

cor = oucal.Calibrator(cal_id=args.cal_id,
                       cal_pars=cfg.cal_pars,
                       cap=cap,
                       cap_pars=cfg.cap_pars)

if args.n_cal_imgs > 0:
    # Old method used on calibrator object:
    # cor.cap_cal_imgs(args.n_cal_imgs)
    while cor.n_cal_imgs < args.n_cal_imgs:
        # This is where you would command the robot to a new pose in eye-in-hand applications
        # robot.move_to(pose_generator.new_pose())
        c = cap.capture()
        cv2.imshow('capture calibration images', c)
        key = cv2.waitKey(1)
        if key != -1:
            print('Trying to add cal image')
            if cor.add_cal_img(c):
                # Example for saving robot flange in base:
                # BF = robot.get_flange_in_base()
                # with (cor.cal_dir / f'BF_{ci:03d}.pickle').open('wb') as f:
                #     pickle.dump(BF, f)
                print('... Success')
            else:
                print(
                    f""" ... Chessboard not found: Need shape {cor.cal_pars.cb_shape}.\n
                    ... Or wrong image format: Was {c.shape}, needs
                    {cor.cap_pars.shape}""")
    cv2.destroyWindow('capture calibration images')

if args.cal:
    cor.calibrate()
if args.rg:
    cal = cor.cal
    from opencv_utils.ray_getter import RayGetter
    rg = RayGetter(cal=cal)
