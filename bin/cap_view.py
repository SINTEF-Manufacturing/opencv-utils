# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2024"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import atexit
import argparse
import sys
import logging

import cv2

from opencv_utils import Capture, CaptureViewer
from opencv_utils.cvimage_viewer import CVImageViewer
from opencv_utils.config_loader import load_config
from opencv_utils.cam_spec import CSSerial


logging.basicConfig(level=logging.INFO)

ap = argparse.ArgumentParser()
ap.add_argument('--serial', type=str, default=None,
                # choices=CSSerial.get_available_cam_serials()
                )

args = ap.parse_args()

cfg = load_config()

available_cs = CSSerial.get_available_cam_serials()

cam_spec = None

if args.serial is not None:
    if args.serial in available_cs:
        cam_spec = CSSerial(args.serial)
    else:
        print(f'Given serial ({args.serial}) is '
              f'not available ({available_cs}).')
        sys.exit(1)
elif cfg is not None:
    if hasattr(cfg, 'cam_spec'):
        if cfg.cam_spec.serial in available_cs:
            cam_spec = cfg.cam_spec
        else:
            print(f'Configured serial ({cfg.cam_spec.serial}) '
                  f'is not available ({available_cs}).')
            sys.exit(1)
    else:
        print(f'Missing serial. Available: ({available_cs}).')
        sys.exit(1)
else:
    print(f'Possible serial numbers: {available_cs}')
    sys.exit(1)

cap_pars = None
if cfg is not None and hasattr(cfg, 'cap_pars'):
    cap_pars = cfg.cap_cls

c = Capture(cam_spec, cap_pars)
c.mono = True
# c.shape = (1200, 800)
# c.shape = (320, 240)
atexit.register(c.stop)
c.start()

iv = CVImageViewer()

cv = CaptureViewer(c, iv)
atexit.register(cv.stop)
cv.start()

