#!/usr/bin/python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import cv2
from cv2 import aruco
import matplotlib.pyplot as plt

shape = (2970, 2100)
a4img = 255 * np.ones(shape, dtype=np.uint8)  # size format of A4 and 10 dpmm
ms = 300  # Marker size
mg = 1  # Margin (must be >0)
long_mid = True  # Should mid-markers be placed on the long-sides?
a_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
#for i in range(4):

a4img[mg:mg+ms, mg:mg+ms] = aruco.drawMarker(a_dict, 0, ms)
a4img[mg:mg+ms, -(mg+ms):-(mg)] = aruco.drawMarker(a_dict, 1, ms)
a4img[-(mg+ms):-(mg), mg:mg+ms] = aruco.drawMarker(a_dict, 2, ms)
a4img[-(mg+ms):-(mg), -(mg+ms):-(mg)] = aruco.drawMarker(a_dict, 3, ms)
if long_mid:
   a4img[(shape[0] - ms)//2:(shape[0] + ms)//2, mg:mg+ms] = aruco.drawMarker(a_dict, 4, ms)
   a4img[(shape[0] - ms)//2:(shape[0] + ms)//2, -(mg+ms):-(mg)] = aruco.drawMarker(a_dict, 5, ms)
 

cv2.imwrite('a4aruco.bmp', a4img)
plt.imshow(a4img, cmap='gray')
# plt.savefig('a4aruco.pdf')
plt.show()

# Test recognition
parameters =  aruco.DetectorParameters_create()
corners, ids, rejectedImgPoints = aruco.detectMarkers(a4img, a_dict, parameters=parameters)
plt.figure()

# Draw detected markers
frame_markers = aruco.drawDetectedMarkers(a4img.copy(), corners, ids)
plt.imshow(frame_markers)
for i in range(len(ids)):
    c = corners[i][0]
    plt.plot([c[:, 0].mean()], [c[:, 1].mean()], 'o', label = f'id={ids[i]}')
plt.legend()
plt.show()
